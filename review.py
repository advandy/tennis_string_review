from bs4 import BeautifulSoup
import requests
baseUrl = "https://www.tennis-warehouse.com"
urls = [
    "https://www.tennis-warehouse.com/BabolatString.html",
    "https://www.tennis-warehouse.com/WilsonString.html",
    "https://www.tennis-warehouse.com/LuxilonString.html",
    "https://www.tennis-warehouse.com/PrinceString.html",
    "https://www.tennis-warehouse.com/TecnifibreString.html",
    "https://www.tennis-warehouse.com/SolincoString.html",
    "https://www.tennis-warehouse.com/GammaString.html",
    "https://www.tennis-warehouse.com/YonexString.html",
    "https://www.tennis-warehouse.com/HeadString.html"

]
csv_file = open("review.csv", "w")
csv_file.write("Name,Price,Power,Spin,Comfort,Control,Touch/Feel,String Movement,Playability Duration,Durability,Overall\n")
for url in urls:
    req = requests.get(url)
    soup = BeautifulSoup(req.content, "html.parser")
    reviews = soup.find_all(class_="review")
    for review in reviews:
        price = review.parent.parent.parent.find(class_="commanum").next
        name = review.parent.parent.parent.find(class_="name").next
        score = {}
        rev_url = baseUrl + review.get("href")
        rev_req = requests.get(rev_url)
        rev_soup = BeautifulSoup(rev_req.content, "html.parser")
        if rev_soup.find(class_="score_box") is not None:
            for i in rev_soup.find(class_="score_box").find(class_="playscore").find_all("tr"):
                if i.find("td") is not None:
                    score[i.find_all("td")[0].next] = i.find_all("td")[1].next
        elif rev_soup.find(class_="review_scores") is not None:
            for i in rev_soup.find(class_="review_scores").find_all("tr"):
                if i.find("td") is not None:
                    score[i.find_all("td")[0].next] = i.find_all("td")[1].next
        touch = score.get("Touch") or score.get("Feel")
        string_mov = score.get("String Movement") or "null"
        if score == {}:
            continue
        csv_file.write(name + "," + price + "," + score.get("Power") + "," + score.get("Spin") + "," + score.get("Comfort")
                       + "," + score.get("Control") + "," + touch + "," + string_mov + ","
                       + score.get("Playability Duration") + "," + score.get("Durability") + "," + score.get("Overall") + "\n")

csv_file.close()

